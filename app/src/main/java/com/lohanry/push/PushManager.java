package com.lohanry.push;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.unity3d.player.UnityPlayer;

import java.util.ArrayList;

import static com.unity3d.player.UnityPlayer.currentActivity;

/**
 * Created by Lohanry on 2017/2/15.
 */

public class PushManager {
    private static ArrayList<Intent> mPendingIntentListArray = new ArrayList<>();
    public static void setPush(BasePush push){
        mPendingIntentListArray.add(push.getAlarmIntent());
        push.getAlarm().setAlarm();
    }
    public static void cancelPush(BasePush push){
        mPendingIntentListArray.add(push.getAlarmIntent());
        push.getAlarm().cancelAlarm();
    }
    public static void cancelAll(){
        //理论上还需要取消所有的闹钟
        AlarmManager mAlarmManager = (AlarmManager)currentActivity.getSystemService(Context.ALARM_SERVICE);
        for (int i = 0; i < mPendingIntentListArray.size(); i++) {
            mAlarmManager.cancel((PendingIntent) mPendingIntentListArray.get(i));
        }
        NotificationManager notificationManager = (NotificationManager) currentActivity.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }
}
