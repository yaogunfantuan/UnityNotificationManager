package com.lohanry.push;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

/**
 * Created by Lohanry on 2017/2/15.
 */

public abstract class BaseAlarm {

    protected abstract Context getContext();
    protected abstract Intent getIntent();
    protected abstract int getAlarmNotificationID();
    protected abstract AlarmManager getAlarmManager();
    protected abstract int getTimeType();
    protected abstract int getTimeMillis();
    protected abstract int getBroadcastFlags();
    protected abstract int getExecuteMode();

    public void setAlarm(){
        PendingIntent mPendingIntent = PendingIntent.getBroadcast(getContext(),getAlarmNotificationID(),getIntent(),getBroadcastFlags());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            switch (getExecuteMode()) {
                case 0: //setExactAndAllowWhileIdle
                    getAlarmManager().setExactAndAllowWhileIdle(getTimeType(), getTimeMillis(), mPendingIntent);
                    break;
                case 1: //setExact
                    getAlarmManager().setExact(getTimeType(), getTimeMillis(), mPendingIntent);
                    break;
                case 2:
                    getAlarmManager().set(getTimeType(), getTimeMillis(), mPendingIntent);
                    break;
            }
        }
        else{
            getAlarmManager().set(getTimeType(), getTimeMillis(), mPendingIntent);
        }
    }
    public  void cancelAlarm(){
        PendingIntent mPendingIntent = PendingIntent.getBroadcast(getContext(),getAlarmNotificationID(),getIntent(),getBroadcastFlags());
        getAlarmManager().cancel(mPendingIntent);
    }
}
