package com.lohanry.Instance;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.lohanry.push.BaseAlarm;

/**
 * Created by Lohanry on 2017/2/15.
 */

public class ExampleAlarm extends BaseAlarm{
    public ExampleAlarm(Context context, Intent intent){

    }

    @Override
    protected Context getContext() {
        return null;
    }

    @Override
    protected Intent getIntent() {
        return null;
    }

    @Override
    protected int getAlarmNotificationID() {
        return 0;
    }

    @Override
    protected AlarmManager getAlarmManager() {
        return null;
    }

    @Override
    protected int getTimeType() {
        return 0;
    }

    @Override
    protected int getTimeMillis() {
        return 0;
    }

    @Override
    protected int getBroadcastFlags() {
        return 0;
    }

    @Override
    protected int getExecuteMode() {
        return 0;
    }
}
