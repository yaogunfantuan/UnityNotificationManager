package com.lohanry.unitynotification;

/**
 * Created by Lohanry on 2017/2/15.
 */

public interface BaseAlarm {
    UnityAlarm getAlarm();
}
