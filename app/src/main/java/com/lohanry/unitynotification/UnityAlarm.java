package com.lohanry.unitynotification;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

/**
 * Created by Tony on 2017/2/14.
 */

public class UnityAlarm {

    private AlarmManager mAlarmManager;
    private Intent mIntent;
    private PendingIntent mPendingIntent;
    private Context mContext;
    private int id;

    private int BroadcastFlags;
    private int ExecuteMode;
    private int TimeType;
    private Long TimeMillis;

    public UnityAlarm(Context mContext){
        this.mContext = mContext;
        mAlarmManager = (AlarmManager) this.mContext.getSystemService(Context.ALARM_SERVICE);
    }

    public UnityAlarm(Context mContext, Intent intent){
        this(mContext);
        this.mIntent = intent;

    }

    public AlarmManager getmAlarmManager() {
        return mAlarmManager;
    }

    public void setIntent(){

    }

    public void setID(int id){
        this.id = id;
    }

    public void setExecuteMode(int executeMode) {
        this.ExecuteMode = executeMode;
    }

    public void setTimeType(int timeType) {
        this.TimeType = timeType;
    }

    public void setTimeMillis(Long timeMillis) {
        TimeMillis = timeMillis;
    }

    /**
     * 设置PendingIntent.getBroadcast()获得的模式
     *  FLAG_ONE_SHOT：唯一取出，只可取出一次
     *  FLAG_NO_CREATE,：有值取出，无值返Null
     *  FLAG_CANCEL_CURRENT：如果已经存在，则在产生新的Intent之前会先取消掉当前的。
     *  FLAG_UPDATE_CURRENT：取出有的更新
     *  说明：https://my.oschina.net/yaogunfantuan/blog/838762
     * */
    public void setBroadcastFlags(int broadcastFlags) {
        this.BroadcastFlags = broadcastFlags;
    }

    public void setAlarm(){
        if (mPendingIntent == null){
            mPendingIntent =  PendingIntent.getBroadcast(this.mContext,id,mIntent,this.BroadcastFlags);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            switch (this.ExecuteMode) {
                case 0: //setExactAndAllowWhileIdle
                    mAlarmManager.setExactAndAllowWhileIdle(this.TimeType, this.TimeMillis, mPendingIntent);
                    break;
                case 1: //setExact
                    mAlarmManager.setExact(this.TimeType, this.TimeMillis, mPendingIntent);
                    break;
                case 2:
                    mAlarmManager.set(this.TimeType, this.TimeMillis, mPendingIntent);
                    break;
            }
        }
        else
            mAlarmManager.set(this.TimeType, this.TimeMillis, mPendingIntent);
    }
    public void setAlarm(int TimeType,Long timeMillis){
        setTimeType(TimeType);
        setTimeMillis(timeMillis);
        setAlarm();
    }
}
